const express = require("express");
const app = express();

/**
 * Import module express (line 1)
 * Instance express (line 2)
 */

app.get("/", (req, res) => {
  const body = `
    <h2>Landing Page</h2>
    <a href="http://localhost:3000/catalog">Catalog</a> |
    <a href="http://localhost:3000/contact">Contact Us</a> |
    <a href="http://localhost:3000/page-not-found">Another Route</a>
    `;
  res.send(body);
});

/**
 * Route untuk landing page => Landing Page (header)
 * Route untuk catalog page => Catalog Page (header)
 * Route untuk contact page => Contact Us (header)
 * Route untuk page not found => 404: Page Not Found (header)
 *
 * Listen server running at port 3000
 */

app.get("/catalog", (req, res) => {
  res.end("Catalog");
});

app.get("/contact", (req, res) => {
  res.end("Contact Us");
});

app.get("/page-not-found", (req, res) => {
  res.end("404: Page Not Found");
});

app.listen(3000, () => {
  console.log(`Server running at port 3000`)
});

