// Middleware

const express = require("express");
const app = express();

const logger = (req, res, next) => {
  console.log(`${req.method} ${req.url}`);
  next();
};

app.use(logger);

// Setiap request GET ke http://localhost:300/ akan diarahkan ke handler ini
app.get("/", (req, res) => {
  res.end("Hello World");
});

// Setiap request GET ke http://localhost:300/products akan diarahkan ke handler ini
app.get("/products", (req, res) => {
  res.json(["Pen", "Pencil", "Book"]);
});

// Setiap request GET ke http://localhost:300/orders akan diarahkan ke handler ini
app.get("/orders", (req, res) => {
  res.json([
    {
      id: 1,
      paid: false,
      user_id:1,
    },
    {
      id: 2,
      paid: true,
      user_id: 1,
    }
  ]);
});

app.listen(3000, () => {
  console.log(`Server running at port 3000`)
});






