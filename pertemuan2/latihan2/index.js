/**
 * Todo : ubah isi teks pada file text.txt menjadi kalimat di bawah ini:
 * Recap pelajaran hari ini:
 * 1. Modul HTTP adalah modul yang digunakan untuk bekerja dengan protokol HTTP
 * 2. Modul HTTP adalah modul build-in di Node.js
 * 3. HTTP memiliki 2 (dua) jenis transaksi data, yaitu HTTP request dan HTTP response
 * 4. Status code adalah kode berupa angka dari 100+ hingga 500+
 */

 const fs = require("fs");
 fs.writeFileSync("./text.txt", `Recap pelajaran hari ini:
 1. Modul HTTP adalah modul yang digunakan untuk bekerja dengan protokol HTTP
 2. Modul HTTP adalah modul build-in di Node.js
 3. HTTP memiliki 2 (dua) jenis transaksi data, yaitu HTTP request dan HTTP response
 4. Status code adalah kode berupa angka dari 100+ hingga 500+`);

 const text = fs.readFileSync("./text.txt", "utf-8");
 
 console.log(text);