const express = require("express");
const app = express();
const users = [];

app.set("view engine", "ejs");
app.use(express.urlencoded({extended: false}));

app.get("/", (req, res) => {
  res.send(`Jumlah user ${users.length}`);
  // res.render("index");
});

// http://localhost:3000/greet diarahkan ke handler ini
app.get("/greet", (req, res) => {
  const name = req.query.name || "Void";
  res.render("greet", {
    name,
  });
});

app.get("/register", (req, res) => {
  res.render("register");
});

app.post("/register", (req, res) => {
  const {email, password} = req.body;

  users.push({email, password});
  res.redirect("/");
});

app.listen(3000, () => {
  console.log("Server running at port 3000")
});





