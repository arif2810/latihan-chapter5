// Import core module
const https = require("https");
const fs = require("fs");

const options = {
  key: fs.readFileSync("./key.pem"),
  cert: fs.readFileSync("./cert.pem"),
};

https
  .createServer(options, function(req, res){
    res.end("Hello World");
  })
  .listen(8000);


// openssl genrsa 1024 > key.pem
// openssl req -x509 -new -key key.pem > cert.pem
// node index.js
// curl https://localhost:8000 -k

// Output Hello World

